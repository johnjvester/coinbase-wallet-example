# `coinbase-wallet-example` Repository

> The `coinbase-wallet-example` repository is simple web3 Dapp example built upon [React](https://reactjs.org/) and
> [web3](https://web3js.readthedocs.io/en/v1.3.0/) to illustrate how easy it is to get started with the [Coinbase Wallet](https://www.coinbase.com/wallet).

## Publications

This repository is related to a DZone.com publication:

* [Exploring the Coinbase API From a Web2 Starting Point](https://dzone.com/articles/exploring-the-coinbase-api-from-a-web2-starting-po)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using This Repository

At a high level, the following steps are required to utilize this repository:

1. Create a new project in (Infura)[https://infura.io/]
2. Update the `App.js` and the `DEFAULT_ETH_JSONRPC_URL` with the Ropsten HTTP URL
3. `npm install` or `npm ci`
4. `yarn start` or `npm start`

Navigate to http://localhost:3000 to view the Dapp.

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.

