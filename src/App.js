import React, { useEffect, useState } from 'react';
import './App.css';
import CoinbaseWalletSDK from '@coinbase/wallet-sdk'
import Web3 from 'web3';

const APP_NAME = 'coinbase-wallet-example';
const APP_LOGO_URL = './coinbase-logo.png';
const DEFAULT_ETH_JSONRPC_URL = 'https://ropsten.infura.io/v3/56f ... d69'; // Replace with your own Infura.io project
const DEFAULT_CHAIN_ID = 3; // 1=Ethereum (mainnet), 3=Ropsten, 5=Gorli
const DEFAULT_ETHEREUM_CHAIN_ID = '0x3'; // Should match DEFAULT_CHAIN_ID above, but with leading 0x

let DONATION_ADDRESS = '0x7 ... c94'; // Target donation address goes here, just as a simple example

const App = () => {
  const [isWalletConnected, setIsWalletConnected] = useState(false);
  const [account, setAccount] = useState();
  const [walletSDKProvider, setWalletSDKProvider] = useState();
  const [web3, setWeb3] = useState();
  const [responseMessage, setResponseMessage] = useState();

  useEffect(() => {
    const coinbaseWallet = new CoinbaseWalletSDK({
      appName: APP_NAME,
      appLogoUrl: APP_LOGO_URL,
    });

    const walletSDKProvider = coinbaseWallet.makeWeb3Provider(
        DEFAULT_ETH_JSONRPC_URL,
        DEFAULT_CHAIN_ID
    );

    setWalletSDKProvider(walletSDKProvider);

    const web3 = new Web3(walletSDKProvider);
    setWeb3(web3);
  }, []);

  const checkIfWalletIsConnected = () => {
    if (!window.ethereum) {
      console.log(
          'No ethereum object found. Please install Coinbase Wallet extension or similar.'
      );

      web3.setProvider(walletSDKProvider.enable());

      return;
    }

    console.log('Found the ethereum object:', window.ethereum);

    connectWallet();
  };

  const connectWallet = async () => {
    const accounts = await window.ethereum.request({
      method: 'eth_requestAccounts',
    });

    if (!accounts.length) {
      console.log('No authorized account found');
      return;
    }

    if (accounts.length) {
      const account = accounts[0];
      console.log('Found an authorized account:', account);
      setAccount(account);

      try {
        await window.ethereum.request({
          method: 'wallet_switchEthereumChain',
          params: [{ chainId: DEFAULT_ETHEREUM_CHAIN_ID }],
        });
        console.log('Successfully switched to Ropsten Network');
      } catch (error) {
        console.error(error);
      }
    }

    setIsWalletConnected(true);
  };

  const donate = async () => {
    if (!account || !window.ethereum) {
      console.log('Wallet is not connected');
      return;
    }

    // For this simple example, make the DONATION_ADDRESS the user's address (send to themselves)
    DONATION_ADDRESS = account;

    const donationAmount = document.querySelector('#donationAmount').value;

    web3.eth.sendTransaction({
      from: account,
      to: DONATION_ADDRESS,
      value: donationAmount
    }, function(err, transactionHash) {
      if (!err) {
        setResponseMessage(transactionHash + ' success');
        console.log(transactionHash + " success");
      } else {
        console.error(err);
      }
    });
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={APP_LOGO_URL} className="App-logo" alt="logo" />
        {isWalletConnected ? (
            <>
              <h4>Donate some funds to yourself</h4>
              <p>Connected Account: {account}</p>
              <div>
                <input
                    type="number"
                    id="donationAmount"
                    defaultValue={1.00}
                />
                <label htmlFor="donationAmount">WEI</label>
                <button onClick={donate} id="donate" type="button">
                  Donate
                </button>
              </div>
            </>
        ) : (
            <button onClick={checkIfWalletIsConnected} id="connect" type="button">
              Connect Wallet
            </button>
        )}
        <p>{responseMessage}</p>
      </header>
    </div>
  );
}

export default App;
